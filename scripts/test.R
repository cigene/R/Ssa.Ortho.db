
con <- dbConnect(SQLite(), dbname= "inst/extdata/ortho.sqlite")

get.tree <- function(x, tree = 'gene'){
    if(!tree %in% c('gene','clan','clan.rooted'))
        stop("ERROR: tree has to be one of: 'gene','clan','clan.rooted' ")
    ## choose table
    tab <- switch(tree,
                  gene = {"GeneTree"},
                  clan = {"ClanTree"},
                  clan.rooted = {"ClanTreeRooted"})

    if(all(c(length(x) == 1, x == "*"))){
        query <- paste("SELECT Id, Tree FROM", tab)
    }else{
        ## Join the x vector to string 
        x2 = paste(x, sep = ", ", collapse = "', '")
        x = paste("('", x2, "')", sep = "")
        query <- paste("SELECT Id, Tree FROM", tab,
                       "WHERE Id IN", x)
    }
    ## Run query
    q <- dbGetQuery(con, query)
    if(nrow(q) == 0)
        stop(paste("ERROR: could not find", x, "in", tab))
    qq <- lapply(q$Tree, 'unserialize')
    names(qq) <- q$Id
    return(qq)
}


a <- get.tree(c("OG0000009_1","OG0000009_2"), tree = "clan")





get.ortho <- function(x, type = "name"){
    if(!type %in% c('name','symbol','gene_id','protein_id'))
        stop("ERROR: type has to be one of: 'name','symbol',gene_id','protein_id")
    if(all(c(length(x) == 1, x == "*"))){
        query <- paste("SELECT * FROM Link li 
                        LEFT JOIN Gene ge
                        ON ge.oid = li.oid")                       
    }else{
        if(type == "name"){
            if(length(x) > 1)
                stop("ERROR: Can only look for one name at a time")
            query <- paste("SELECT * FROM Link li 
                           LEFT JOIN Gene ge
                           ON ge.oid = li.oid
                           WHERE ge.description LIKE '%",x,"%'", sep ="")
        }else if(type == "symbol"){
            if(length(x) > 1)
                stop("ERROR: Can only look for one name at a time")
            query <- paste("SELECT * FROM Link li 
                           LEFT JOIN Gene ge
                           ON ge.oid = li.oid
                           WHERE ge.symbol LIKE '%",x,"%'", sep ="")
        }else if(type == 'gene_id'|type == 'protein_id'){
            ## Join the x vector to string 
            x2 = paste(x, sep = ", ", collapse = "', '")
            x = paste("('", x2, "')", sep = "")
            id <- switch(type,
                  gene_id = {"ge.gene_id"},
                  protein_id = {"ge.peptide_id"})
            query <- paste("SELECT * FROM Link li 
                           LEFT JOIN Gene ge
                           ON ge.oid = li.oid
                           WHERE",id,
                           "IN", x)    
        }
    }
    ## Run query
    q <- dbGetQuery(con, query)
    if(nrow(q) == 0)
        stop(paste("ERROR: could not find", x))
    return(q)
}


get.dplyr <- function(x = "Gene"){
    if(! x %in% c("Link","Gene"))
        stop("ERROR: Has to be Liink or Gene")
    ## dplyr connect to db
    db <- src_sqlite( "inst/extdata/ortho.sqlite")
    xtbl <- tbl(db, x)
    return(xtbl)
}

ll <- get.dplyr("Link")
gg <- get.dplyr()

j <- left_join(ll, gg, copy = TRUE)


             


## Insert tree data as blob


headcon <- dbConnect(SQLite(), dbname= "inst/extdata/ortho.sqlite")

a <- "tnf"


out <- dbGetQuery(con, "SELECT * FROM Link JOIN ")
    

   

       
    
    


        
    }else{
        query <- paste("SELECT tree FROM", tab,
                       "WHERE id IN ('")
    }
}






SELECT * 
FROM Link li 
LEFT JOIN Gene ge
ON ge.oid = li.oid
WHERE ge.description LIKE '%TOLL%'



                     library(ape)
a <- dbGetQuery(db, "SELECT tree FROM trees where OMCLgroup == 'OG0000000'")

b <- lapply(a$tree, 'unserialize')

plot(b[[1]])



##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## MAKE the SQLite DB
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library(RSQLite)

                                        #setup the DB
db <- dbConnect(SQLite(), dbname= db.name)

                                        # insert Tables into DB
db.finished <- vector()
db.finished[1] <- dbWriteTable(conn = db, name = "GTF", value = gtf, row.names=FALSE) 
db.finished[2] <- dbWriteTable(conn = db, name = "CDS", value = cds, row.names=FALSE) 
db.finished[3] <- dbWriteTable(conn = db, name = "PFAM", value = pfam, row.names=FALSE) 
db.finished[4] <- dbWriteTable(conn = db, name = "GENES", value = annot, row.names=FALSE) 
db.finished[5] <- dbWriteTable(conn = db, name = "HITS", value = hit.df, row.names=FALSE) 
db.finished[6] <- dbWriteTable(conn = db, name = "GO", value = go, row.names=FALSE) 
db.finished[7] <- dbWriteTable(conn = db, name = "SNP", value = snp, row.names=FALSE) 
# Kegg stuff
db.finished[8] <- dbWriteTable(conn = db, name = "KEGGK", value = kegg.knr, row.names=FALSE)
db.finished[9] <- dbWriteTable(conn = db, name = "KEGGP", value =  kegg.kpath, row.names=FALSE)
db.finished[10] <- dbWriteTable(conn = db, name = "KEGGN", value = kegg.kname, row.names=FALSE) 

# FINAL CHECK
cat("-----------------------------------------------------------------\n")
if(all(db.finished) == TRUE){
    cat("STEP 9: making .DB --> FINISHED\n")
}else{
    cat("ERROR: DB is not complete! Table(s) are missing\n")
}
