#' Retrieve trees
#' 
#' \code{get.tree(x, tree = "gene")}
#'
#'
#' @param x A vector of Gene od Clan Tree identifyers, using "*" will return
#'          all results
#' @param tree Defaults to gene (GeneTrees), other options are: 
#'             'clan' for ClanTrees or 'clan.rooted' for rooted
#'             ClanTrees.
#' @return Returns a list of ape::phylo tree objects
#' @examples
#' clantrees <- get.tree(c("OG0000009_1","OG0000009_2"), tree = "clan")
#' require(ape)
#' plot(clantrees[[1]])
#' 
#' genetrees <- get.tree(c("OG0023726"))
#' require(ape)
#' plot(genetrees[[1]])
#'
#' @export

get.tree <- function(x, tree = 'gene'){
    if(!tree %in% c('gene','clan','clan.rooted'))
        stop("ERROR: tree has to be one of: 'gene','clan','clan.rooted' ")
    ## choose table
    tab <- switch(tree,
                  gene = {"GeneTree"},
                  clan = {"ClanTree"},
                  clan.rooted = {"ClanTreeRooted"})

    if(all(c(length(x) == 1, x == "*"))){
        query <- paste("SELECT Id, Tree FROM", tab)
    }else{
        ## Join the x vector to string 
        x2 = paste(x, sep = ", ", collapse = "', '")
        x = paste("('", x2, "')", sep = "")
        query <- paste("SELECT Id, Tree FROM", tab,
                       "WHERE Id IN", x)
    }
    ## Run query
    q <- dbGetQuery(.ortho_db$con, query)
    if(nrow(q) == 0)
        stop(paste("ERROR: could not find", x, "in", tab))
    qq <- lapply(q$Tree, 'unserialize')
    names(qq) <- q$Id
    return(qq)
}
