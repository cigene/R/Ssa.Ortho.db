#' Retrieve ortholog gene informatio
#' 
#' \code{get.ortho(x, type = "name")}
#'
#'
#' @param x Some kind of identifier/ name, using "*" will return
#'          all results
#' @param type Type of identifier used in 'x'. Defaults to name (gene name): 
#'             this will return all rows where the gene name contains x. 
#'             Other options are symbol (gene symbol) and gene_id or protein_id. 
#'             In case of gene_id / protein_id x can be a vector of idetifiers.  
#' @return Returns a data.frame
#' @examples
#' ## Find all orthologue clusters containing 'toll' genes
#' a <- get.ortho("toll")
#'
#' @export


get.ortho <- function(x, type = "name"){
    if(!type %in% c('name','symbol','gene_id','protein_id'))
        stop("ERROR: type has to be one of: 'name','symbol',gene_id','protein_id")
    if(all(c(length(x) == 1, x == "*"))){
        query <- paste("SELECT ge.*, li.OMCLgroup, li.OMCLclan
                        FROM Link li 
                        LEFT JOIN Gene ge
                        ON ge.oid = li.oid")                       
    }else{
        if(type == "name"){
            if(length(x) > 1)
                stop("ERROR: Can only look for one name at a time")
            query <- paste("SELECT ge.*, li.OMCLgroup, li.OMCLclan
                           FROM Link li 
                           LEFT JOIN Gene ge
                           ON ge.oid = li.oid
                           WHERE ge.description LIKE '%",x,"%'", sep ="")
        }else if(type == "symbol"){
            if(length(x) > 1)
                stop("ERROR: Can only look for one name at a time")
            query <- paste("SELECT ge.*, li.OMCLgroup, li.OMCLclan
                           FROM Link li 
                           LEFT JOIN Gene ge
                           ON ge.oid = li.oid
                           WHERE ge.symbol LIKE '%",x,"%'", sep ="")
        }else if(type == 'gene_id'|type == 'protein_id'){
            ## Join the x vector to string 
            x2 = paste(x, sep = ", ", collapse = "', '")
            x = paste("('", x2, "')", sep = "")
            id <- switch(type,
                  gene_id = {"ge.gene_id"},
                  protein_id = {"ge.peptide_id"})
            query <- paste("SELECT ge.*, li.OMCLgroup, li.OMCLclan
                           FROM Link li 
                           LEFT JOIN Gene ge
                           ON ge.oid = li.oid
                           WHERE",id,
                           "IN", x)    
        }
    }
    ## Run query
    q <- dbGetQuery(.ortho_db$con, query)
    if(nrow(q) == 0)
        stop(paste("ERROR: could not find", x))
    return(q)
}
