- [Quick start](#sec-1)
  - [Install](#sec-1-1)
  - [What Info can you find](#sec-1-2)


# Quick start<a id="orgheadline3"></a>

You can track (and contribute to) development of Ssa.RefSeq.db at <https://gitlab.com/fabian.grammes/org.Ssalar.eg.db>

## Install<a id="orgheadline1"></a>

1.  Install the release version of devtools from CRAN with `` `install.packages("devtools")` ``

2.  Install `` `org.Ssalar.eg.db` `` from GitLab using:

```R
library(devtools)
install_git('https://gitlab.com/fabian.grammes/Ssa.ortho.db')
```

## What Info can you find<a id="orgheadline2"></a>

Todo
